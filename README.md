# Prog-Test

## Pro prváky (i ostatní)
Je dost možný, že máte zatím v hlavě guláš a nevíte jestli se vám tohle může hodit, jak to nainstalovat a používat.


**NEBOJTE** se mi napsat na Discord (krystof18) nebo na [prasikry@fit.cvut.cz](mailto:prasikry@fit.cvut.cz), rád vám pomůžu.


**Žádný dotaz není hloupý!**


Všichni jsme si prošli začátky kdy jsme byli zmatení a **je to normální**

## Goal
This repo was created mainly for MAC users who cannot use Valgrind natively on their systems.

It provides simple way to test you programs against test files and also Valgrind memcheck tests.
## Requirements

You need to have install Docker for MAC (I believe only the command line version doesn't come with QEMU - thus probably won't work).

To install docker for MAC, follow the instructions here: https://docs.docker.com/desktop/install/mac-install/


## Installation

I don't have time to mess with Brew and such stuff, so you will have to link the prog-test file to you /usr/local/bin/ directory yourself.

1. Clone this repo to your computer.
2. run `sudo ln -s /path/to/this/dir/prog-test /usr/local/bin/prog-test`

Then you can run the prog-test command from anywhere in your terminal.

## Usage
To use this tool, simply copy the `CZE` folder from test files you download from progtest.fit.cvut.cz to root of your project directory.
Then run `prog-test` command in your project directory. 

If you'd like to configure the tool with your own options, read the next section.

## Custom options
You can set custom options for the prog-test command by creating a file called `prog-test.env` in your project directory.
This file will overwrite the default options.
All the options and their default values can be found in `prog-test.env.example` file.

## Troubleshooting
And if you have any trouble with installation or usage, please message me on Discord (krystof18) or email me at [prasikry@fit.cvut.cz](mailto:prasikry@fit.cvut.cz) and I will try to help you as soon as possible.

## Contribution
If you want to contribute, feel free to do so. I will be happy to review and merge your MRs.

If you have any experience with putting tools to Brew or any other package managers and you'd like to help, message me please.
