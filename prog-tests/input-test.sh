#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
ORANGE='\033[0;33m'
BLUE='\033[0;34m'
PROGRAM='/tmp/test/a.out'
TEST_PATH='/tmp/test/CZE/*_in.txt'

if [ -f /tmp/test/prog-test.env ]; then
  source /tmp/test/prog-test.env
fi

for IN_FILE in $TEST_PATH; do
  REF_FILE=$(echo -n "$IN_FILE" | sed -e 's/_in/_out/');
  echo -e "${BLUE}Test: $IN_FILE${NC}";
  echo -e "${ORANGE}Reference output${REF_FILE}${NC}";
  $PROGRAM < "$IN_FILE" > /tmp/test/my_out.txt;
  if ! diff "$REF_FILE" /tmp/test/my_out.txt; then
    echo -e "${RED}Fail: $IN_FILE${NC}";
    exit 1;
  else
    echo -e "${GREEN}OK: $IN_FILE${NC}";
  fi
done
