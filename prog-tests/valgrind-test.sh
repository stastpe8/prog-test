#!/bin/bash

RED='\033[0;31m'
GREEN='\033[0;32m'
NC='\033[0m' # No Color
ORANGE='\033[0;33m'
PROG=/tmp/test/a.out
VALGRIND_OPTIONS="--leak-check=full --show-leak-kinds=all"
if [ -f /tmp/test/prog-test.env ]; then
  source /tmp/test/prog-test.env
fi

for IN_FILE in /tmp/test/CZE/*_in.txt; do
  VALGRIND_OUT=$(valgrind "$VALGRIND_OPTIONS" $PROG < "$IN_FILE" --error-exitcode=5 2>&1)
  if [ $? -eq 5 ]; then
    echo -e "${RED}Fail: $IN_FILE${NC}";
    echo -e "${ORANGE}Valgrind output:${NC}\n\n";
    echo -e "$VALGRIND_OUT\n\n";
    exit 1;
  else
    echo -e "${GREEN}VALGRIND OK: $IN_FILE${NC}";
  fi
done
